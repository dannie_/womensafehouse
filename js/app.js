var myVar;

function myLoader() {
	myVar = setTimeout(showPage, 5000);
}


function showPage() {
	document.getElementById("loader").style.display = "none";
	document.getElementById("hero").style.display = "flex";
}

window.onload(myLoader());

//accordion

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
	acc[i].addEventListener("click", function () {
		this.classList.toggle("active");
		var panel = this.nextElementSibling;
		if (panel.style.maxHeight) {
			panel.style.maxHeight = null;
		} else {
			panel.style.maxHeight = panel.scrollHeight + "px";
		}
	});
}


//slideout for nav
function openNav() {
	document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
	document.getElementById("myNav").style.width = "0%";
}